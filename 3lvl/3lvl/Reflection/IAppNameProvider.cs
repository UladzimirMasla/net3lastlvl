﻿using System.Threading.Tasks;

namespace _3lvl.Reflection
{
    interface IAppNameProvider
    {
        Task<string> GetAppName();
    }
}
