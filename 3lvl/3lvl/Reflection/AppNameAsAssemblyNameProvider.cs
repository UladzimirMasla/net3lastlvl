﻿using System.Reflection;
using System.Threading.Tasks;

namespace _3lvl.Reflection
{
    class AppNameAsAssemblyNameProvider : IAppNameProvider
    {
        [InvokeMe]
        public Task<string> GetAppName()
        {
            var assemblyName = Assembly.GetEntryAssembly().GetName().Name;

            return Task.FromResult(assemblyName);
        }
    }
}
