﻿using System.Threading.Tasks;

namespace _3lvl.Reflection
{
    class ConstAppNameProvider : IAppNameProvider
    {
        const string _appName = "My application";

        public Task<string> GetAppName()
        {
            return Task.FromResult(_appName);
        }
    }
}
