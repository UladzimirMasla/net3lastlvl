﻿using System;

namespace _3lvl.Reflection
{
    [AttributeUsage(AttributeTargets.Method)]
    class InvokeMeAttribute : Attribute 
    {
    }
}
