﻿using System.IO;
using System.Threading.Tasks;

namespace _3lvl.Reflection
{
    class AppNameFromFileProvider : IAppNameProvider
    {
        private string _filePath;


        public AppNameFromFileProvider(string filePath)
        {
            _filePath = filePath;
        }


        public async Task<string> GetAppName()
        {
            using (var reader = new StreamReader(_filePath))
            {
                return await reader.ReadToEndAsync();
            }
        }
    }
}
