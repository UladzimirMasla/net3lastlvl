﻿using System;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using _3lvl.Reflection;
using _3lvl.Threading;
using _3lvl.WrappersForAnOldAsyncModels;

namespace _3lvl
{

    class Program
    {
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern int MessageBox(IntPtr hWnd, string lpText, string lpCaption, uint uType);


        static async Task Main(string[] args)
        {
            // await RaceCondition.StartRace();

            //MessageBox(IntPtr.Zero, "Invoked Message box", "Hello there!", 0);

            //await FindTypeImplementingInterfaceAndInvokeOne(typeof(IAppNameProvider));

            var webClient = new WebClient();
            var result = await webClient.MyDownloadStringTaskAsync(new Uri("https://www.lipsum.com/"));
            Console.WriteLine(result);
        }

        static async Task FindTypeImplementingInterfaceAndInvokeOne(Type typeToFind)
        {
            var foundImplementations = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(inst => typeToFind.IsAssignableFrom(inst) && !inst.IsInterface);

            foreach(var impl in foundImplementations)
            {
                Console.WriteLine(impl.FullName);
            }

            var constImpl = foundImplementations.First(impl => impl.FullName.Contains("ConstAppNameProvider"));
            var constInstance = Activator.CreateInstance(constImpl) as IAppNameProvider;

            var appName = await constInstance.GetAppName();
            Console.WriteLine($"Got a reply from ConstAppNameProvider: {appName}");

            var methodToExecute = foundImplementations.SelectMany(impl => impl.GetMethods())
                            .First(m => m.GetCustomAttributes(typeof(InvokeMeAttribute), false).Length > 0);

            var classInstance = Activator.CreateInstance(methodToExecute.DeclaringType, null);

            var appNameFromMarkedAttribute = await (methodToExecute.Invoke(classInstance, null) as Task<string>);
            Console.WriteLine($"Got a reply from marked method: {appNameFromMarkedAttribute}");
        }
    }

}

//  Advanced Multi-threading
//      Advanced thread synchronization (ManualResetEvent, AutoResetEvent, ReadWriteLock, Interlocked, Volatile)
//      Async / await in depth
//        Task cancellation
//      TaskCompletionSource
//      SynchronizationContext (what, why, how)
//  Interaction with unmanaged code basics (invoke WinAPI with P/invoke)
//  Dynamic typing
//  Reflection.
//      Find types implementing an interface in an assembly
//      Instantiate an object of a type
//      Invoke a method marked with an attribute


