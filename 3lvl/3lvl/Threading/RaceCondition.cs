﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace _3lvl.Threading
{
    static class RaceCondition
    {
        public static async Task StartRaceBadWay()
        {
            int counter = 0;

            await Task.WhenAll(Enumerable.Range(0, 10).Select(_ => Task.Run(() =>
            {
                for(var i = 0; i < 10000; i++)
                {
                    counter++;
                }
            })));


            // expect counter to be = 100000
            Console.WriteLine($"Got counter = {counter}");
        }

        public static async Task StartRaceMutex()
        {
            int counter = 0;
            var mutex = new Mutex();


            var watch = new Stopwatch();
            watch.Start();

            await Task.WhenAll(Enumerable.Range(0, 10).Select(_ => Task.Run(() =>
            {
                for (var i = 0; i < 10000; i++)
                {
                    mutex.WaitOne();
                    counter++;
                    mutex.ReleaseMutex();
                }
            })));

            watch.Stop();

            // expect counter to be = 100000
            Console.WriteLine($"Got counter = {counter}, time spend {watch.ElapsedMilliseconds}");
        }

        public static async Task StartRaceSemaphore()
        {
            int counter = 0;
            var semaphore = new Semaphore(1,1);

            var watch = new Stopwatch();
            watch.Start();

            await Task.WhenAll(Enumerable.Range(0, 10).Select(_ => Task.Run(() =>
            {
                for (var i = 0; i < 10000; i++)
                {
                    semaphore.WaitOne();
                    counter++;
                    semaphore.Release();
                }
            })));

            watch.Stop();

            // expect counter to be = 100000
            Console.WriteLine($"Got counter = {counter}, time spend {watch.ElapsedMilliseconds}");
        }

        public static async Task StartRaceLock()
        {
            int counter = 0;
            var lockObject = new object();

            var watch = new Stopwatch();
            watch.Start();

            await Task.WhenAll(Enumerable.Range(0, 10).Select(_ => Task.Run(() =>
            {
                for (var i = 0; i < 10000; i++)
                {
                    lock(lockObject)
                    {
                        counter++;
                    }
                }
            })));

            watch.Stop();

            // expect counter to be = 100000
            Console.WriteLine($"Got counter = {counter}, time spend {watch.ElapsedMilliseconds}");
        }

        public static async Task StartRaceMonitor()
        {
            int counter = 0;
            var lockObject = new object();

            var watch = new Stopwatch();
            watch.Start();

            await Task.WhenAll(Enumerable.Range(0, 10).Select(_ => Task.Run(() =>
            {
                for (var i = 0; i < 10000; i++)
                {
                    Monitor.TryEnter(lockObject, TimeSpan.FromSeconds(3));
                    try
                    {
                        counter++;
                    }
                    finally
                    {
                        Monitor.Exit(lockObject);
                    }
                }
            })));

            watch.Stop();

            // expect counter to be = 100000
            Console.WriteLine($"Got counter = {counter}, time spend {watch.ElapsedMilliseconds}");
        }

        public static async Task StartRaceWithReaderWriterLock()
        {
            int counter = 0;
            var rwl = new ReaderWriterLock();

            var watch = new Stopwatch();
            watch.Start();

            var readValueTask = Task.Run(() =>
            {
                for (var i = 0; i < 10; i++)
                {
                    rwl.AcquireReaderLock(TimeSpan.FromSeconds(3));
                    Console.WriteLine($"Current counter = {counter}");
                    rwl.ReleaseReaderLock();

                    Thread.Sleep(1);
                }
            });

            var writeTasks = Enumerable.Range(0, 10).Select(_ => Task.Run(() =>
            {
                for (var i = 0; i < 10000; i++)
                {
                    rwl.AcquireWriterLock(TimeSpan.FromSeconds(3));
                        counter++;
                    rwl.ReleaseWriterLock();
                }
            }));

            await Task.WhenAll(Enumerable.Append(writeTasks, readValueTask));

            watch.Stop();

            // expect counter to be = 100000
            Console.WriteLine($"Got counter = {counter}, time spend {watch.ElapsedMilliseconds}");
        }

        public static async Task StartRaceWithInterlocked()
        {
            int counter = 0;

            var watch = new Stopwatch();
            watch.Start();

            var readValueTask = Task.Run(() =>
            {
                for (var i = 0; i < 10; i++)
                {
                    Console.WriteLine($"Current counter = {counter}");

                    Thread.Sleep(1);
                }
            });

            var writeTasks = Enumerable.Range(0, 10).Select(_ => Task.Run(() =>
            {
                for (var i = 0; i < 10000; i++)
                {
                    Interlocked.Increment(ref counter);
                }
            }));

            await Task.WhenAll(Enumerable.Append(writeTasks, readValueTask));


            watch.Stop();

            // expect counter to be = 100000
            Console.WriteLine($"Got counter = {counter}, time spend {watch.ElapsedMilliseconds}");
        }
    }
}
